var UrlBasePostgre = "http://postgrebc9688.cloudapp.net:3000/api/usuarios/";
var UrlBasePostgreLogin = "http://postgrebc9688.cloudapp.net:3000/api/login";
var UrlBaseMongo = "https://api.mlab.com/api/1/databases/leasblog/collections/leasposts/";
var ApiKeyMongo = "CP1BFstvQarv3XNqZMGaT_I3Fisuz7lO";

var leas={
    /** @namespace */
  util:{
      /** Espara milisegundos
       * @param {int} pMilliSeconds - Cadena que hay que borrar 'leas_'.
      */
       wait: function(pMilliSeconds) {
          var counter= 0
              , start = new Date().getTime()
              , end = 0;
          while (counter < pMilliSeconds) {
              end = new Date().getTime();
              counter = end - start;
          }
      },
      /** Limpia la cadena 'leas_'.
       * @param {string} pStirng - Cadena que hay que borrar 'leas_'.
      */
      borraLeas: function(pStirng){
          return pStirng.substr(5)
      },
      /** Añade la cadena 'leas_'.
       * @param {string} pStirng - Cadena que hay que añadir 'leas_'.
       * @returns  cadena de entrada pon el prefijo leas_
      */
      ponLeas: function(pString){
        return "LEAS_" + pString;
      },
      /** Fecha actual
       * @returns  Devuelve fecha actual en formato dd/mm/yyyy hh:mm:ss
      */
      getToday: function () {
          var hoy = new Date();
          dia = hoy.getDate();
          mes = hoy.getMonth();
          anio= hoy.getFullYear();
          hora = hoy.getHours();
          minuto = hoy.getMinutes();
          seg = hoy.getSeconds();
          return String(dia+"/"+mes+"/"+anio +" "+ hora+":"+minuto+":"+seg);
        },

      geoloc: function(){
          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(this.showPosition);
            }
        },
      showPosition: function (position) {
          console.log("this.geoLat1",position.coords.latitude);
          this.geoLat = position.coords.latitude;
          this.geoLong = position.coords.longitude;
        }
  },
  /** @namespace */
  user:{
    /** Busca el usuario logado
     * @returns {string}  código de usuario logado
    */
    getUserLogin:function(){
      return sessionStorage.getItem('usuario');
    },
    /** Realiza el Login y almacena en sesión "usuario" el nombre del usuario
     * @param {string} pUser - Usuario
     * @param {string} pPassword -Contraseña
     * @returns {bool}  exito de la transacción
    */
    login: function(pUser, pPassword){
          var peticion = new XMLHttpRequest();
          peticion.open("POST", UrlBasePostgreLogin, false);
          peticion.setRequestHeader("Content-Type", "application/json");
          peticion.setRequestHeader("Access-Control-Allow-Origin", "*");
          peticion.send( '{"nombre":"' + pUser + '", "password": "' + pPassword + '"}');
          var respuesta = JSON.parse(peticion.responseText);
          if (peticion.status=="200" && respuesta.data.count > 0)
          {
              sessionStorage.setItem("usuario",leas.util.borraLeas(pUser));
              return true;
          }
          else
          {
              sessionStorage.removeItem("usuario");
              return false;
          }
    },
    /** Realiza el logout y borra sesión "usuario"
    */
    logout:function(){

      sessionStorage.removeItem("usuario");
    },
    /** Devuelve todos los usuarios
     * @returns {bool}  JSON con todos los usuarios
    */
    getAll:function(){
      var LosUsuarios;
      var peticion = new XMLHttpRequest();
      peticion.open("GET",UrlBasePostgre,false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();
      return leas.user.filterLEAS(JSON.parse(peticion.responseText).data);
    },
    /** Crea usuarios en la base de datos
     * @param {string} pNombre - Nombre de usuario
     * @param {string} pEmail -Correo electrónico del usuario
     * @param {string} pPassword -Contraseña del usuario
     * @returns {bool}  exito de la transacción
    */
    create:function(pNombre,pEmail,pPassword) {
      var nombre = "LEAS_" + pNombre;
      var email = pEmail;
      var password = pPassword;
      var peticion = new XMLHttpRequest();
      peticion.open("POST", UrlBasePostgre, false);
      peticion.setRequestHeader("Content-Type", "application/json");
      peticion.setRequestHeader("Access-Control-Allow-Origin", "*");
      peticion.send('{"nombre":"' + nombre + '", "email": "' + email + '", "password": "' + password + '"}');
      var respuesta = JSON.parse(peticion.responseText);
      if (peticion.status=="200")
      {
          return true;
      }
      else
      {
        return false;
      }
    },
    /** Verifica si hay usuario logado
     * @returns {bool}  si hay o no usuario logado
    */
    isLogin:function() {
      if (sessionStorage.getItem("usuario") == null) {
        return false;
      }else {
        return true;
      }
    },
    /** Borra un usuario
     * @param {string} pId - Id del usuario a borrar
    */
    delete:function(pId){
      console.log("delete");
      var peticion = new XMLHttpRequest();
      URLItem = UrlBasePostgre + pId;
      peticion.open("DELETE",URLItem,false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();
    },
    /** Actualiza los datos de unsuario, si un dato no está se borra
     * @param {string} pNombre - Nombre de usuario
     * @param {string} pEmail -Correo electrónico del usuario
     * @param {string} pPassword -Contraseña del usuario
     * @param {string} pId -Contraseña del usuario
     * @returns {bool}  exito de la transacción
    */
    update:function(pUser,pEmail,pPassword,pId){
      var URLItem = UrlBasePostgre + pId ;//+ "/";
      console.log(URLItem);
      var peticion1 = new XMLHttpRequest();
      peticion1.open("PUT",URLItem,true);
      peticion1.setRequestHeader("Content-Type","application/json");
      var strSend=
          "{nombre:'" + pUser +
           "',email:'" + pEmail +
           "',password:'" + pPassword +
             "'}" ;
      peticion1.send('{"nombre":"' + pUser + '", "email": "' + pEmail + '", "password": "' + pPassword + '"}');
    },
    updateArray: function(pArray,pUser,pEmail,pPassword,pIdUsuario){
      console.log("pArray",pArray);
      for (var i = 0; i < pArray.length; i++) {
        if (pArray[i].id == pIdUsuario) {
          pArray[i].id = pIdUsuario;
          pArray[i].nombre = pUser;
          pArray[i].email = pEmail;
          pArray[i].password = pPassword;
          return pArray;
        }
      }

    },


    /** Filtra de un array de usuario los que contienen el prefijo 'leas_'
     * @param {array} pUsuarios - Array de usuario a filtrar
     * @returns {array}  array con los usuarios filtrados
    */
    filterLEAS:function(pUsuarios){
      var LosUsuarios = [];
      for (var i = 0; i < pUsuarios.length; i++) {
        var a = JSON.stringify(pUsuarios[i]);
        if (a.split(",")[1].split(":")[1].indexOf("LEAS") == 1) {
          LosUsuarios.push(pUsuarios[i]);
        }
      }
      return LosUsuarios;
    },
    /** Devuelve un usuario
     * @param {string} pId - ID de usuario
     * @returns {json}  Usuario coincidente
    */
    get: function(pId){
      var peticion = new XMLHttpRequest();
      peticion.open("GET",UrlBasePostgre + pId,false);
      peticion.setRequestHeader("Content-Type","application/json");
      peticion.send();
      return JSON.parse(peticion.responseText).data;
    },
  },
  /** Conjunto de junciones para manejar los posts
  @namespace */
  post:{

     /** Busca el primer post en el arry de json y devuelve un de JSON
      * @param {string} pId - Id del post a buscar
      * @param {string} mPosts - Array de posts a buscar
      * @returns {array}  array con los usuarios filtrados
     */
    find:function(pId, mPosts){
      for (var i = 0; i < mPosts.length; i++) {
          var element = mPosts[i];
          var _id = element._id;
          if (_id == undefined) {
             return "";
           }else {
             if (element._id.$oid == pId) {
                return element;

           }
        }
    }},
    /** Save a post
     * @param {string} pTitulo - Head of the post
     * @param {string} pTexto - Post
     * @param {string} pIdUsuario - Id Session User
     * @returns {string}  ID of the new post
    */
    save:function(pTitulo,pTexto,pIdUsuario){
      var peticion = new XMLHttpRequest();
      var URLItem = UrlBaseMongo + "?apiKey=" + ApiKeyMongo;
      peticion.open("POST", URLItem, false);
      peticion.setRequestHeader("Content-Type", "application/json");
      var _lat = 40.502084 + (Math.random()/100) ;
      var _long =  -3.664929  - (Math.random()/100);
      var strSend = "{'titulo': '"+pTitulo+
                    "','texto':'"+pTexto+
                    "','idusuario':'" + pIdUsuario +
                    "','lat':'" + _lat +
                    "','long':'" + _long +
                    "','fechamodificacion': '" + leas.util.getToday() +
                    "','fechaalta': '" + leas.util.getToday() +
                    "'}";
      peticion.send(strSend);
      return JSON.parse(peticion.responseText)._id.$oid;
    },
    /** Save a post
     * @param {string} pTitulo - Head of the post
     * @param {string} pTexto - Post
     * @param {string} pId - Id Post
     * @param {string} pIdUsuario -User name session
     * @param {string} pFechaCreacion - Creation Date
     * @param {string} pLat - latitude
     * @param {string} pLong - length
    */
    update: function(pId,pTitulo,pTexto,pIdUsuario,pFechaCreacion,pLat,pLong) {
        var peticion = new XMLHttpRequest();
        var URLItem = UrlBaseMongo;
        URLItem += pId ;
        URLItem += "?apiKey="+ ApiKeyMongo;
        peticion.open("PUT", URLItem, false);
        peticion.setRequestHeader("Content-Type", "application/json");
        var strSend=
             "{titulo:'" + pTitulo +
             "',texto:'" + pTexto +
             "','lat':'" + pLat +
             "','long':'" + pLong +
             "',idusuario:'" + pIdUsuario +
             "',fechaalta:'" + pFechaCreacion +
             "'}" ;
        peticion.send(strSend);
      },
    /** Get all post
     * @returns {string}  JSON with all post
    */
    getAll:function(){
        var peticion = new XMLHttpRequest();
        URLItem = UrlBaseMongo + "?apiKey="+ ApiKeyMongo;
        peticion.open("GET",URLItem,false);
        peticion.setRequestHeader("Content-Type","application/json");
        peticion.send();

        return JSON.parse(peticion.responseText);
      },
    /** Delete a post
      * @param {string} pId - Id of the post
    */
    delete: function(pId){
      var url = UrlBaseMongo + pId +"?apiKey="+ ApiKeyMongo;
      var peticion = new XMLHttpRequest();
      peticion.open("DELETE", url, true);
      peticion.send();
    }
  }
}
