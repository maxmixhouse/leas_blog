FROM node:boron
#FROM microsoft/nanoserver

#
#CREAR DIRECTORIO DE LA APP
#
ENV carpeta /usr/src/app
RUN mkdir -p carpeta
RUN mkdir -p /usr/src/app/src
WORKDIR /usr/src/app
#
# Instalar dependencias
#
RUN apt-get update && \
    apt-get install -y --no-install-recommends git && \
    apt-get clean && \
    npm install -g gulp bower polymer-cli generator-polymer-init-custom-build
COPY package.json /usr/src/app
#
# Empaquetar código
#
COPY . /usr/src/app
COPY ./src /usr/src/app/src
#
# Publico el puerto
#
EXPOSE 8081
#
# Arrancro la aplicacion
#
CMD [ "polymer" , "serve", "--hostname", "0.0.0.0" ]
#
